@RestResource(urlMapping='/claimsmanager/*')

global with sharing class REST_ClaimsManager {
    
    @HttpPost
    global static void claimById() {
        
        RestRequest request = RestContext.request;
        String requestBody = request.requestBody.toString(); 
        System.debug(requestBody);
        RestResponse restResponse = RestContext.response;
        REST_ClaimsResponseBean response = REST_IntegrationHandler.getClaimsResponseInformation(requestBody);
        
        restResponse.addHeader('Content-Type', 'Application/JSON');
        restResponse.responseBody = Blob.valueOf(JSON.serialize(response, true));
        System.debug(JSON.serialize(response, true));
    }
    
}