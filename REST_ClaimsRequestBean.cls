/**=================================================================================================================================
* Created By: 
* Created Date: 22/02/2021
* Description: Represents Claims Schema for get, create and update

* Change History:
* None
===================================================================================================================================**/

public class REST_ClaimsRequestBean {
    
    Public String RequestType;
    Public String Origin; // Email or MyOMI 
    Public String SalesforceCaseGUID; //Uniquely identifies Salesforce Case ID
    Public String SalesforceCaseNumber; //Unique auto number for the claim case
    Public String CaseStatus; //Claim Case Status
    Public String ExternalGUID; //External GUID that synapse can store in Salesforce to process a case in the future
    Public String claimNumber;
    
    Public String MessageIdentifier;
    Public String policyNumber;
    Public String HoardBatchId;
    Public String ThreadIdentifier;
    Public String FromAddress;
    Public String CcAddress;
    Public String ToAddress;
    Public String BccAddress;
    Public String Subject;
    Public String HtmlBody;
    Public integer externalReference;
    Public integer costOfClaim;
    Public string dateOfLoss;
    Public integer incidentTime;
    Public integer latestEstimate;
    Public integer latestEstiamteOD;
    Public String description;
    Public String dateOfRegistration;
    Public String audits;
    Public String branch;
    Public String division;
    Public integer riskAddress;
    Public String status;
    Public integer catastropheCode;
    Public String catastropheDescription;
    Public integer claimBrokerReferenceNumber;
    Public integer legalReferenceNumber;
    Public integer siuReferenceNumber;
    Public String notificationSource;
    Public String notificationSourceDescription;
    Public String redCarpetIndicator;
    Public integer policyHolderId;
    Public String notificationDate;
    Public integer notificationType;
    Public String informerType;
    Public String informerTypeDescription;
    Public String informerName;
    Public integer informerContactNumber;
    Public String lossDescription;
    Public String nofifyEmail;
    Public String brokerInformation;
    Public String premiumOverride;
    Public String scheduleReceived;
    Public String chLossOfUseOverride;
    Public String isGroupScheme;
    Public integer brokerNumber;
    Public String clientName;
    Public integer clientNumber;
    Public integer treatyCode;
    Public String treatyDescription;
    Public String riskAddressDescription;
    Public String handler;
    Public String sourceSystem;
    Public String declined;
    Public String declinedReason;
    Public integer nameIdNumber;
    Public integer subcaseNumber;
    Public String isMotor;
    Public integer riskNumber;
    Public String driverName;
    Public integer sapCaseNumber;
    Public String sapOffice;
    Public String policyHolderName;
    Public integer objectSequenceNumber;
    Public integer itemNumber;
    Public String eventType;

    //New fields for live chat questions
    Public String unattendedVehicle;
    Public String violentEntry;
    Public String reportToPolice;
    Public String PoliceStationCaseNumber;
    Public String isSaria;
    Public String dateOfIncident;
    Public String SuburbOfIncident;
    Public String alternativeContact;
    
}