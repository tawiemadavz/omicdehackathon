public class REST_IntegrationHandler {
    
    public static REST_ClaimsResponseBean getClaimsResponseInformation(String requestBodyP){ 
        
        //Deserialize the request body into the RESTClaimsRequestBean class
        REST_ClaimsRequestBean claimCase = (REST_ClaimsRequestBean)JSON.deserialize(requestBodyP, REST_ClaimsRequestBean.class);
        System.debug('Claim Request ClaimNumber: ' + claimCase.claimNumber);
        
        REST_ClaimsResponseBean response = REST_IntegrationHandler.ProcessClaimRequestBean(claimCase); 
        System.debug('Response in getClaimsResponseInformation : ' + response);

                
        
         
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {'ram@tetrad.co.za'}; 
        mail.setToAddresses(toAddresses);
        mail.setSenderDisplayName('Testing PL Stuff');
        mail.setSubject(claimCase.RequestType);
        mail.setPlainTextBody(requestBodyP);
        //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
        
        return response;
    }
    
    public static REST_ClaimsResponseBean ProcessClaimRequestBean(REST_ClaimsRequestBean claimRequestP){ 
        
        REST_ClaimsResponseBean newResponseBean = new REST_ClaimsResponseBean();
        System.debug('Claim Request Type :' + claimRequestP.RequestType);
        System.debug('Claim Request Body :' + claimRequestP);
        
        if(claimRequestP != null) {

            //----------Added for Hackathon API to check claim status when provided with claim number-----------//
            if(claimRequestP.RequestType == 'Claim_Check'){
                List<Claim> ClaimToProcessList = [Select Id, Name, Status from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                String claimStatus;
                for(Claim cl : ClaimToProcessList){
                    claimStatus = CL.Status;
                }

                if(ClaimToProcessList.isEmpty()){
                    newResponseBean.ResponseMessage = 'We cannot find a claim with this number on the system.';
                    newResponseBean.StatusCode = 400;   
                    System.debug('Response final : ' +newResponseBean);
                }
                else{
                    newResponseBean.ResponseMessage = claimStatus;
                    newResponseBean.StatusCode = 200;   
                    System.debug('Response final : ' +newResponseBean);
                }
                return newResponseBean;
            }

            //-------------------------------------End--------------------------------------------//
            
            if(claimRequestP.RequestType == 'Claim_Header_Registered'){
                
                if(claimRequestP.SalesforceCaseNumber == null){
                    Case insertedCase = REST_Integration_Utils.createClaimCase(claimRequestP.Subject, 
                                                                               claimRequestP.HtmlBody,
                                                                               claimRequestP.claimNumber, 
                                                                               'Opened',
                                                                               claimRequestP.policyNumber
                                                                              );
                    newResponseBean.ResponseMessage = 'New Claim Reqeust Created';
                    newResponseBean.StatusCode = 201;
                }else if (claimRequestP.SalesforceCaseNumber != null){
                    Case claimCase = [Select ID from Case Where CaseNumber =: claimRequestP.SalesforceCaseNumber ORDER BY CreatedDate Desc Limit 1];
                    if(claimCase.Id != null){
                        Claim claimToProcess = [Select Id, OMI_Claim_Number__c from Claim where CaseId =: claimCase.Id];
                        if(claimToProcess.Id != null){
                            claimToProcess.OMI_Claim_Number__c = claimRequestP.claimNumber;
                            update claimToProcess;
                        }
                    }
                }
                
            }
            
            if(claimRequestP.RequestType == 'TIA_Case_Added' && claimRequestP.claimNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                System.debug('process list: '+ClaimToProcessList);
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted'; 
                        ClaimToProcess.Claim_Sub_Status__c = 'TIA Case Added';
                        ClaimToProcess.Current_User__c = UserInfo.getUserId();
                        System.debug('Claim Status :' + ClaimToProcess.Status);
                        //REST_IntegrationHandler_FNOL.REST_GetClaimCaseInformation(ClaimToProcess.Id);
                        Update ClaimToProcess;
                        
                        newResponseBean.ResponseMessage = 'Claim Processed';
                        newResponseBean.StatusCode = 200;   
                        System.debug('Response final : ' +newResponseBean);
                        return newResponseBean;
                    }
                }
                
            }
            
            if(claimRequestP.RequestType == 'Questions_Answered' && claimRequestP.claimNumber != null){
                //Claim ClaimToProcess = REST_Integration_Utils.getClaimUsingOMIClaimNumber(claimRequestP.claimNumber);
                
                List<Claim> ClaimToProcessList = [Select Id, Name, Unattended_Vehicle_Ext__c,
                 Violent_Entry_Ext__c, Report_To_Police_Ext__c, Is_Saria_Ext__c, Alternative_Contact_Ext__c,
                  Incident_Suburb_Ext__c, Police_Station_Case_Number__c, Item_Claimed_Ext__c from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted'; 
                        ClaimToProcess.Claim_Sub_Status__c = 'Questions Answered';
                        ClaimToProcess.Current_User__c = UserInfo.getUserId();
                        //Live chat questions
                        //Check if atleast one of those fields are in the request
                        if(claimRequestP.unattendedVehicle != null){
                            ClaimToProcess.Unattended_Vehicle_Ext__c = (claimRequestP.unattendedVehicle.toUpperCase() == 'YES' || claimRequestP.unattendedVehicle.toUpperCase() == '1') ? true : false;
                            ClaimToProcess.Violent_Entry_Ext__c = (claimRequestP.violentEntry.toUpperCase() == 'YES' || claimRequestP.violentEntry.toUpperCase() == '1') ? true : false;
                            ClaimToProcess.Report_To_Police_Ext__c = (claimRequestP.reportToPolice.toUpperCase() == 'YES' || claimRequestP.reportToPolice.toUpperCase() == '1') ? true : false;
                            ClaimToProcess.Is_Saria_Ext__c = (claimRequestP.isSaria.toUpperCase() == 'YES' || claimRequestP.isSaria.toUpperCase() == '1') ? true : false;
                            ClaimToProcess.Alternative_Contact_Ext__c = claimRequestP.alternativeContact;
                            ClaimToProcess.Incident_Suburb_Ext__c = claimRequestP.SuburbOfIncident;
                            ClaimToProcess.Police_Station_Case_Number__c = claimRequestP.PoliceStationCaseNumber;
                        }
                        
                        System.debug('Claim Status :' + ClaimToProcess.Status);
                        Update ClaimToProcess;
                        
                        newResponseBean.ResponseMessage = 'Claim Processed';
                        newResponseBean.StatusCode = 200;   
                        System.debug('Response final : ' +newResponseBean);
                        return newResponseBean;
                    }
                }
                newResponseBean.ResponseMessage = 'Claim Not Found';
                newResponseBean.StatusCode = 200;   
                System.debug('Response final : ' +newResponseBean);
                return newResponseBean;
                
            }
            
            if(claimRequestP.RequestType == 'Claim_Submitted'){
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Submitted'; 
                        ClaimToProcess.Claim_Sub_Status__c = '';
                        ClaimToProcess.Current_User__c = UserInfo.getUserId();
                        System.debug('Claim Status :' + ClaimToProcess.Status); 
                        Update ClaimToProcess;
                        
                        newResponseBean.ResponseMessage = 'Claim Processed';
                        newResponseBean.StatusCode = 200;   
                        System.debug('Response final : ' +newResponseBean);
                        return newResponseBean;
                    }
                }
                newResponseBean.ResponseMessage = 'Claim Not Found';
                newResponseBean.StatusCode = 200;   
                System.debug('Response final : ' +newResponseBean);
                return newResponseBean;
                
            }
            
            if(claimRequestP.RequestType == 'Email'){
                
                if(claimRequestP.SalesforceCaseNumber != null){
                    Case ExistingCase = [Select Id from Case where CaseNumber =: claimRequestP.SalesforceCaseNumber];
                    
                    REST_Integration_Utils.createEmailMessage(claimRequestP.Subject, claimRequestP.FromAddress, claimRequestP.ToAddress,
                                                              claimRequestP.CcAddress, claimRequestP.BccAddress, claimRequestP.HtmlBody,
                                                              claimRequestP.MessageIdentifier, ExistingCase.Id );
                }
                if(claimRequestP.claimNumber != null){
                    Claim claim = [Select ID, CaseId from Claim where Omi_Claim_Number__c =: claimRequestP.claimNumber Order By CreatedDate Desc Limit 1];
                    System.debug('Email Type Claim : ' + claim);
                    Case ExistingCase = [Select Id from Case where Id =: claim.CaseId];
                    System.debug('Email Type Existing Case : ' + ExistingCase);
                    
                    REST_Integration_Utils.createEmailMessage(claimRequestP.Subject, claimRequestP.FromAddress, claimRequestP.ToAddress,
                                                              claimRequestP.CcAddress, claimRequestP.BccAddress, claimRequestP.HtmlBody,
                                                              ExistingCase.Id, ExistingCase.Id );
                }
                if(claimRequestP.policyNumber != null){ 
                    
                    System.debug('Email Claim Registration for Policy Number: ' +claimRequestP.policyNumber);
                    
                    Case insertedCase = REST_Integration_Utils.createClaimCase(claimRequestP.Subject, 
                                                                               claimRequestP.HtmlBody,
                                                                               claimRequestP.claimNumber, 
                                                                               'Opened',
                                                                               claimRequestP.policyNumber
                                                                              );
                    
                    newResponseBean.ResponseMessage = 'Claim Created from Email for Policy Number : ' + claimRequestP.policyNumber;
                    newResponseBean.StatusCode = 200;   
                    return newResponseBean;
                }
            }  
            
            if(claimRequestP.RequestType == 'MFT_Supplier_Appointed'){
                
                //Claim ClaimToProcess = REST_Integration_Utils.getClaimUsingOMIClaimNumber(claimRequestP.claimNumber);
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    System.debug('Claim ' + ClaimToProcess);
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Supplier Appointed';
                        System.debug('Claim Status :' + ClaimToProcess.Status);
                        ClaimToProcess.Current_User__c = UserInfo.getUserId();
                        Update ClaimToProcess;
                        
                        
                        newResponseBean.ResponseMessage = 'Claim Processed';
                        newResponseBean.StatusCode = 200;   
                        System.debug('Response final : ' +newResponseBean);
                        return newResponseBean;
                    }
                }
                newResponseBean.ResponseMessage = 'Claim Not Found';
                newResponseBean.StatusCode = 200;   
                System.debug('Response final : ' +newResponseBean);
                return newResponseBean;
                
            }  
            
            if(claimRequestP.RequestType == 'MFT_Quote_Started'){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    System.debug('Claim ' + ClaimToProcess);
                    System.debug('Claim ' + ClaimToProcess);
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Quote Started';
                        ClaimToProcess.Received_in_Bizagi__c = system.today();
                        System.debug('Claim Status :' + ClaimToProcess.Status);
                        ClaimToProcess.Current_User__c = UserInfo.getUserId();
                        Update ClaimToProcess;
                        
                        newResponseBean.ResponseMessage = 'Claim Processed';
                        newResponseBean.StatusCode = 200;   
                        System.debug('Response final : ' +newResponseBean);
                        return newResponseBean;
                    }
                } 
                newResponseBean.ResponseMessage = 'Claim Not Found';
                newResponseBean.StatusCode = 200;   
                System.debug('Response final : ' +newResponseBean);
                return newResponseBean;
            }
            
            if(claimRequestP.RequestType == 'A_DRP_Sent'){
                
                
            } 
            
            if(claimRequestP.RequestType == 'A_Quote_Accepted'){
                
                
            } 
            
            if(claimRequestP.RequestType == 'G_Supplier_Appointed'){
                
                  List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    System.debug('Claim ' + ClaimToProcess);
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Supplier Appointed';
                        System.debug('Claim Status :' + ClaimToProcess.Status);
                        ClaimToProcess.Current_User__c = UserInfo.getUserId();
                        Update ClaimToProcess;
                        
                        
                        newResponseBean.ResponseMessage = 'Claim Processed';
                        newResponseBean.StatusCode = 200;   
                        System.debug('Response final : ' +newResponseBean);
                        return newResponseBean;
                    }
                }
                newResponseBean.ResponseMessage = 'Claim Not Found';
                newResponseBean.StatusCode = 200;   
                System.debug('Response final : ' +newResponseBean);
                return newResponseBean;
              
                
                
            } 
            
            if(claimRequestP.RequestType == 'G_Repair_Done'){
                
                
            } 
            
            if(claimRequestP.RequestType == 'DRP_Assessment_Sent' && claimRequestP.claimNumber != null && claimRequestP.policyNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted';  
                        ClaimToProcess.Audatex_Status__c = 'DRP Assessment Sent';
                        Update ClaimToProcess;
                        //newResponseBean = REST_Integration_Utils.buildClaimsManagerResponse('Claim Updated Succesfully', 200);
                        //return newResponseBean;
                    }
                }
                
            }
            
            
            
            if(claimRequestP.RequestType == 'DRP_Quote_Approved' && claimRequestP.claimNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted';  
                        ClaimToProcess.Audatex_Status__c = 'DRP Quote Approved';
                        ClaimToProcess.Current_User__c = UserInfo.getUserId();
                        Update ClaimToProcess;
                        
                    }
                }
                
                
            }
            
            
            
            if(claimRequestP.RequestType == 'DRP_Quote_Authorised' && claimRequestP.claimNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted';  
                        ClaimToProcess.Audatex_Status__c = 'DRP Quote Authorised';
                        ClaimToProcess.Current_User__c = UserInfo.getUserId();
                        Update ClaimToProcess;
                        
                    }
                }
                
                
            }
            
            
            if(claimRequestP.RequestType == 'DRP_Repairs_Complete_(Final_Repair_Cost)' && claimRequestP.claimNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted';  
                        ClaimToProcess.Audatex_Status__c = 'DRP Repairs Complete';
                        ClaimToProcess.Current_User__c = UserInfo.getUserId();
                        Update ClaimToProcess;
                        
                    }
                }
                
            }
            
            
            if(claimRequestP.RequestType == 'DRP_Assessment_Cancelled' && claimRequestP.claimNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted';  
                        ClaimToProcess.Audatex_Status__c = 'DRP Assessment Cancelled';
                        Update ClaimToProcess;
                        
                    }
                }
                
            }
            
            
            if(claimRequestP.RequestType == 'Audatex_Total_Loss' && claimRequestP.claimNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted';  
                        ClaimToProcess.Audatex_Status__c = 'Audatex Total Loss';
                        Update ClaimToProcess;
                        
                    }
                }
                
                
            }
            
            
            if(claimRequestP.RequestType == 'Request_Final_Costing' && claimRequestP.claimNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted';  
                        ClaimToProcess.Audatex_Status__c = 'Request Final Costing';
                        Update ClaimToProcess;
                        
                    }
                }
                
            }
            
            
            if(claimRequestP.RequestType == 'DRP_Quote_Received_' && claimRequestP.claimNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted';  
                        ClaimToProcess.Audatex_Status__c = 'DRP Quote Received';
                        Update ClaimToProcess;
                        
                    }
                }
            }
            
            
            if(claimRequestP.RequestType == 'DRP_Quote_Under_Review' && claimRequestP.claimNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted';  
                        ClaimToProcess.Audatex_Status__c = 'DRP Quote Under Review';
                        Update ClaimToProcess;
                        
                    }
                }
            }
            
            
            if(claimRequestP.RequestType == 'External_Assessment' && claimRequestP.claimNumber != null){
                
                List<Claim> ClaimToProcessList = [Select Id, Name from Claim Where OMI_Claim_Number__c =: claimRequestP.claimNumber];
                
                if(ClaimToProcessList != null && ClaimToProcessList.size() >= 1) {
                    Claim ClaimToProcess = ClaimToProcessList[0];
                    
                    System.debug('Claim ' + ClaimToProcess); 
                    if(ClaimToProcess.ID != null) {
                        ClaimToProcess.Status = 'Claim Unsubmitted';  
                        ClaimToProcess.Audatex_Status__c = 'External Assessment';
                        Update ClaimToProcess;
                        
                    }
                }
                
            }
            
            if(claimRequestP.RequestType == 'Towing'){
                
                //Create Case
                try{
                    Case claimCase = REST_Integration_Utils.createClaimCase(claimRequestP.Subject, 
                                                                            claimRequestP.HtmlBody,
                                                                            claimRequestP.claimNumber, 
                                                                            'Opened',
                                                                            claimRequestP.policyNumber
                                                                           );
                    Case claimCaseRecord = [Select Id,CaseNumber,Claim_Number__c,Policy_Number__c,OwnerId from Case where Id =: claimCase.Id limit 1];
                    system.debug('====claimCaseRecord====='+claimCaseRecord);
                    
                    claim claimRecord = new claim();
                    claimRecord.CaseId = claimCaseRecord.Id;
                    claimRecord.Name = claimCaseRecord.CaseNumber;
                    claimRecord.OMI_Claim_Number__c = claimCaseRecord.Claim_Number__c;
                    claimRecord.OMI_Policy_Number__c = claimCaseRecord.Policy_Number__c;
                    claimRecord.RecordTypeId = Schema.SObjectType.Claim.getRecordTypeInfosByName().get('Towing Claim').getRecordTypeId();
                    claimRecord.Status = 'Claim Submitted';
                    insert claimRecord;
                    system.debug('===claimRecord===='+claimRecord);
                    
                    Task reviewClaim = new Task();
                    reviewClaim.subject = 'validate Claim details';
                    reviewClaim.WhoId = claimRecord.OwnerId;
                    reviewClaim.WhatId = claimRecord.Id;
                    insert reviewClaim;
                    system.debug('===reviewClaim===='+reviewClaim);
                    
                    newResponseBean.StatusCode = 200;
                    newResponseBean.SalesforceCaseGUID = claimCaseRecord.Id;
                    newResponseBean.ResponseMessage = 'Towing Response is Created';
                } catch (Exception e){
                    newResponseBean.ResponseMessage = e.getMessage();
            		newResponseBean.StatusCode = 400;
                    //System.debug('ERROR: '+e.getMessage());
                }
                
            }
            
            
        } else{
            
            newResponseBean.ResponseMessage = 'Bad Request - Not Processed at all';
            newResponseBean.StatusCode = 400;
            
            System.debug('Response final : ' +newResponseBean);
        }
        return newResponseBean;
    }
}